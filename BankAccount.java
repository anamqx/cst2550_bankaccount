import java.util.concurrent.locks.*;



public class BankAccount
{
    private double balance;
    private Lock lock;
    private Condition checkFundsCondition;


    public BankAccount()
    {
        balance = 0;
	lock = new ReentrantLock();
	checkFundsCondition = lock.newCondition();
    }


    public void deposit(double amount)
    {   lock.lock();
        System.out.print("Depositing " + amount);
        double newBalance = balance + amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;

	checkFundsCondition.signalAll();
	
	lock.unlock();

    }

    public void withdraw(double amount) {
	lock.lock();
	try{
	    
	    while(balance < amount)
		checkFundsCondition.await();
	    System.out.print("Withdrawing " + amount);
	    double newBalance = balance - amount;
	    System.out.println(", new balance is " + newBalance);
	    balance = newBalance;
	    
	}catch(InterruptedException ex){
	}

	finally{
	    lock.unlock();
	}

    }







    
	    public double getBalance()
	    {
		return balance;
	    }

	   
	


	    
	
    }
